# angulo

This is not a real project. This is a eye bird point of view of software cicle 
of life. Here we analize which projects needs to be created, documented or 
integrated with paascual. Don't look for code here.

# cicle of life

## before start

Before start a project you need to think in what to do, how to do it and why.

There are tools to help you in what to do like mockups, diagram generators, use
case and requirements management tools, pen and paper,... (We need to create
a service to help developers to find theese tools for their kind of project
and links to learning paths of theese tools use).

Exists a lot of metodologies explaining how to do software development. There
are tools helping you to use theese metodologies like posits in a wall or
tools where you define a state machines of your develompent cicle and manage
states of your software features. (The sevice from previous paragraph can be
used to find this kind of tools too. Could be usefull some integration with
theese tools to launch tasks like automated tests or generate artifacts when
a ticket/feature/... change to some state)

When you think in how to do a project you need to think in what technology is
the better to resolve your problem. Sometimes you only know a programming
languages and you choose it because is your unique option. (No idea in how
to do your life easier here)

Sometimes you made a project because you can. Other times you do because you
don't know that a project doing what you can do exists. (Make an
awsome-library-finder with links to projects like https://djangopackages.org/)


## starting a project

You need 
* a version control to store your code
* define a project structure or reuse a structure skeleton from other 
  similar project (cookiecutter, gitlab project templates,... there are a lot
  of tools for theese)
* configure your .gitignore or similar in control version tool (https://www.gitignore.io/)
* configure settings for your ide if you haven't done yet
* automate your day by day things like run tests, generate docs or other
  artifacts, launch a linter,... (we need a tool to generate githooks, manage
  theese tools,...)

## A usual developer day

Here a developer has chosen a lot of tools or configurations, so this is only
a example of millions. This is an invented situation, no programmers has been
hurted.

Our developer has started a new scrum sprint focused on performance and have a
lot of features in his backlog. Choose one and move it "doing" lane. 
He uses gitflow tool to generate a new feature branch and start coding. 
He is working in a local environment with a stack composed by django, 
postgresql and celery with rabbitmq. He is working in a refactor of models
because a previous bad design generates a lot of data in DB and expensive slow
queries to consume data. He have been wrote a migration script that generate
new tables, migrate data from old schema and deletes old tables. All is working
like a charm and he feels happy.

But a issue is detected in production that need to be fixed as soon as possible.
He save his changes in his feature branch and start a new hotfix branch with
gitflow. He read the ticket explaining the issue and look in production sentry
for a stack trace and more detailed information to resolve the problem. He
start to write a test to reproduce failure before fix-it, but wait a moment...
* seems like some combination of data is causing the problem
* annonymized data on his local bbdd is not a problem because he has
  read register ids in sentry
* but he has destroyed local data some minutes ago testing schema migration
* he has not written yet a reverse migration from last state
* he start to feel like shit

He start to think in what he can do to fix his problem:
* He can download another annonymized database dump and restore it in his local
  database but this takes three or four hours
* He can write a reverse migration script but data will be created with
  different ids and he can't find then problematic data
* A month ago he was storing a unchanged dump in a local database and a work
  copy, but with actual data size its imposible to him store two database copys.
  Sadly a fast copy from unchanged copy is not an option anymore
* Maybe somebody can give him credentials to another database copy. Maybe
  staging, maybe but least likely production.

What he can do in a ideal world:
* Restore a database from dumps its slower than restore from a binary copy,
  but for make a binary copy you need snapshots or copy will be corrupted. In a
  ideal world manage filesystems with snapshots should be easy to do
* Copy directly from a snapshot filesystem to a remote filestystem in a block
  level is usually faster than do it at file level. In a ideal world faster way
  must be easy to do
* Restore a local snapshot should be inmediate. Snapshots should only store
  changes. So use snapshots in local develompent environment must be easy to do
  for developers without system skills that manage medium to large datasets.
  (create diogenes project to resolve this)